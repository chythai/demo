﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Net;
using System.Text;
using System.Collections.Specialized;
using SaleCycle.DTO;
using Newtonsoft.Json;

using System.Configuration;
using System.Web.Script.Serialization;

namespace SaleCycleApp
{
    public partial class _Default : Page
    {
        protected static string APIUrlLogin = ConfigurationManager.AppSettings["APILogin"];
        protected static string APIUrl;
        string APIpath = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            txtUserName.Focus();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            if (txtPassword.Text == "" || txtPassword.Text == null)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "alert", "alert('" + "ไม่สามารถเข้าใช้งานระบบได้ กรุณา PASSWORD');", true);
                return;
            }

            if (txtUserName.Text == "" || txtUserName.Text == null)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "alert", "alert('" + "ไม่สามารถเข้าใช้งานระบบได้ กรุณา USERNAME');", true);
                return;
            }

            List<UserLoginInfo> EmpObjectAPIHost = GetConnectAPI();

            if (EmpObjectAPIHost.Count > 0)
            {

                APIUrl = "https://localhost:44358";
                
                List<UserLoginInfo> EmpObject = GetLogin(EmpObjectAPIHost[0].Username.ToString(), EmpObjectAPIHost[0].Password.ToString());

                EmpInfo emp = new EmpInfo();

                if (EmpObject.Count > 0)
                {
                    
                    emp.Username = EmpObject[0].Username;

                    emp.Password = EmpObject[0].Password;

                    emp.ActiveFlag = "Y";

                    emp.EmpCode = EmpObject[0].EmpCode;

                    emp.EmpName_TH = EmpObject[0].EmpFname_TH + " " + EmpObject[0].EmpLname_TH;

                    Session["EmpInfo"] = emp;

                    List<EmpRole> EmpRoleObject = getRole();

                    if (EmpRoleObject[0].RoleCode == "TELE") { Response.Redirect("./src/CallInfo/CallInfo.aspx"); } else { Response.Redirect("./src/Main.aspx"); }
                    
                }
            else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "alert", "alert('" + "ไม่สามารถเข้าใช้งานระบบได้ กรุณาติดต่อเจ้าหน้าที่');", true);

                }

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "alert", "alert('" + "ไม่สามารถเข้าใช้งานระบบได้ กรุณาติดต่อเจ้าหน้าที่');", true);

            }

        }

        public List<UserLoginInfo> GetConnectAPI()
        {
            string APIUrlLogin = "https://localhost:44358";
            
            APIpath = APIUrlLogin + "/SaleCycle/api/Userlogin/GetLogin";

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(APIpath);
            
            httpWebRequest.ContentType = "application/json";
            
            httpWebRequest.Method = "POST";
            
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                var user = new UserLoginInfo();
                user.Username = txtUserName.Text;
                user.Password = txtPassword.Text;

                var json = new JavaScriptSerializer().Serialize(user);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                List<UserLoginInfo> userlist = JsonConvert.DeserializeObject<List<UserLoginInfo>>(result);
                return userlist;
            }
            
        }




        
        public List<UserLoginInfo> GetLogin(string SUsername, string SPassword)
        {
            string APIUrlLogin = "https://localhost:44358";

            APIpath = APIUrlLogin + "/SaleCycle/api/Userlogin/GetLogin";

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(APIpath);

            httpWebRequest.ContentType = "application/json";

            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                var user = new UserLoginInfo();
                user.Username = txtUserName.Text;
                user.Password = txtPassword.Text;

                var json = new JavaScriptSerializer().Serialize(user);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }


            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                List<UserLoginInfo> userlist = JsonConvert.DeserializeObject<List<UserLoginInfo>>(result);
                return userlist;
            }

        }


        protected List<EmpRole> getRole()
        {

            EmpInfo empInfo = new EmpInfo();

            empInfo = (EmpInfo)Session["EmpInfo"];


            string APIUrlLogin = "https://localhost:44358";

            APIpath = APIUrlLogin + "/SaleCycle/api/EmpRole/GetRole";

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(APIpath);

            httpWebRequest.ContentType = "application/json";

            httpWebRequest.Method = "POST";


            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                var emprole = new EmpRole();
                emprole.EmpCode = empInfo.EmpCode;

                var json = new JavaScriptSerializer().Serialize(emprole);

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                List<EmpRole> emprole = JsonConvert.DeserializeObject<List<EmpRole>>(result);
                return emprole;
            }

        }
    }
}