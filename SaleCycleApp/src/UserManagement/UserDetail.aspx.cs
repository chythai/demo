﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Net;
using System.Data;
using System.Text;
using System.Collections.Specialized;
using System.Web.UI.WebControls;
using System.Configuration;
using SaleCycle.DTO;
using Newtonsoft.Json;
using SaleCycle.Common;
using System.Globalization;
using System.IO;

namespace SaleCycleApp.src.UserManagement
{
    public partial class UserDetail : System.Web.UI.Page
    {
        protected static int currentPageNumber;
        protected static int PAGE_SIZE = Convert.ToInt32(ConfigurationManager.AppSettings["PAGE_SIZE"]);
        protected static string APIUrl;

        string APIpath = "";
        string Codelist = "";
        Boolean isdelete;

        public Boolean check_UserLogin = false;
        public Boolean check_Role = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                currentPageNumber = 1;

                EmpInfo empInfo = new EmpInfo();

                empInfo = (EmpInfo)Session["EmpInfo"];

                if (empInfo != null)
                {
                    hidEmpCode.Value = empInfo.EmpCode;
                    // APIUrl = "http://localhost:54545";
                    APIUrl = empInfo.ConnectionAPI;
                }
                else
                {
                    Response.Redirect("..\\..\\Default.aspx?flaglogin=_EMPCODENULL");
                }
               
                loadEmp();
                loadEmpRole();
                loadddlRole(ddlRole);
                Hide_Section();
                showUserLogin(true);
            }
        }
        #region event

        protected void showSection_UserLogin_Click(object sender, EventArgs e)
        {
            Hide_Section();
            sectionLoad_UserLogin();
        }

        protected void showSection_Role_Click(object sender, EventArgs e)
        {
            Hide_Section();
            sectionLoad_Role();
        }

        protected void btnEditUser_Click(object sender, EventArgs e)
        {
            UserLoginInfo uid = new UserLoginInfo();

            string respstr = "";

             EmpInfo empInfo = new EmpInfo();

            EmpInfo eInfo = new EmpInfo();

            empInfo = (EmpInfo)Session["EmpInfo"];

            if (empInfo == null)
            {
                Response.Redirect("..\\..\\Default.aspx?flaglogin=_EMPCODENULL");

            }
            else
            {
                using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();

                    data["UserLoginId"] = hidUserLoginId.Value;
                    data["Username"] = txtusernameIns.Text;
                    data["Password"] = txtPasswordIns.Text;
                    data["EmpCode"] = (Request.QueryString["EmpCode"] != null) ? Request.QueryString["EmpCode"].ToString() : "";

                    data["Updateby"] = empInfo.EmpCode;
                    data["Createby"] = empInfo.EmpCode;

                    if (hidUserLoginId.Value != "")
                    {
                        APIpath = APIUrl + "/api/support/UpdateUserLogin";
                    }
                    else
                    {
                        APIpath = APIUrl + "/api/support/InsertUserLogin";
                    }
                    var response = wb.UploadValues(APIpath, "POST", data);

                    respstr = Encoding.UTF8.GetString(response);
                }

                int? cou = JsonConvert.DeserializeObject<int?>(respstr);


                if (cou > 0)
                {

                    if (hidUserLoginId.Value != "")
                    {
                        hidUserLoginId.Value = cou.ToString();
                    }

                    loadEmp();
                   
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "alert", "alert('" + MessageConst._INSERT_SUCCESS + "');", true);



                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "alert", "alert('" + MessageConst._INSERT_ERROR + "');", true);
                }
            }
        }
    
        protected void btnClearEditUser_Click(object sender, EventArgs e)
        {
            txtusernameIns.Text = "";
            txtPasswordIns.Text = "";
        }
        protected void chkEmpRoleAll_Changed(object sender, EventArgs e)
        {
            for (int i = 0; i < gvEmpRole.Rows.Count; i++)
            {
                CheckBox chkall = (CheckBox)gvEmpRole.HeaderRow.FindControl("chkEmpRoleAll");

                if (chkall.Checked == true)
                {
                    HiddenField hidCode = (HiddenField)gvEmpRole.Rows[i].FindControl("hidEmpRoleId");

                    if (Codelist != "")
                    {
                        Codelist += ",'" + hidCode.Value + "'";
                    }
                    else
                    {
                        Codelist += "'" + hidCode.Value + "'";
                    }

                    CheckBox chkEmpRole = (CheckBox)gvEmpRole.Rows[i].FindControl("chkEmpRole");

                    chkEmpRole.Checked = true;
                }
                else
                {
                    CheckBox chkEmpRole = (CheckBox)gvEmpRole.Rows[i].FindControl("chkEmpRole");

                    chkEmpRole.Checked = false;
                }
            }
            hidIdList.Value = Codelist;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            isdelete = DeleteEmpRole();

            loadEmpRole();

            loadddlRole(ddlRole);

            if (!isdelete)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "alert", "alert('กรุณาเลือกรายการที่ต้องการลบ');", true);
            }
        }
        protected void btnSubmitRole_Click(object sender, EventArgs e)
        {
            EmpRole uid = new EmpRole();

            string respstr = "";

            EmpInfo empInfo = new EmpInfo();

            EmpInfo eInfo = new EmpInfo();

            empInfo = (EmpInfo)Session["EmpInfo"];

            if (validateInsertEmpRole())
            {

                if (empInfo == null)
                {
                    Response.Redirect("..\\..\\Default.aspx?flaglogin=_EMPCODENULL");

                }
                else
                {

                    using (var wb = new WebClient())
                    {
                        var data = new NameValueCollection();


                        data["EmpCode"] = (Request.QueryString["EmpCode"] != null) ? Request.QueryString["EmpCode"].ToString() : "";
                        data["RoleCode"] = ddlRole.SelectedValue;
                        data["FlagDelete"] = "N";

                        data["Updateby"] = empInfo.EmpCode;
                        data["Createby"] = empInfo.EmpCode;


                        APIpath = APIUrl + "/api/support/InsertEmpRole";

                        var response = wb.UploadValues(APIpath, "POST", data);

                        respstr = Encoding.UTF8.GetString(response);
                    }

                    int? cou = JsonConvert.DeserializeObject<int?>(respstr);


                    if (cou > 0)
                    {

                        loadddlRole(ddlRole);

                        loadEmpRole();

                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "alert", "alert('" + MessageConst._INSERT_SUCCESS + "');", true);



                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "alert", "alert('" + MessageConst._INSERT_ERROR + "');", true);
                    }
                }
            }
        }

        #endregion

        #region function

        protected Boolean DeleteEmpRole()
        {

            for (int i = 0; i < gvEmpRole.Rows.Count; i++)
            {
                CheckBox checkbox = (CheckBox)gvEmpRole.Rows[i].FindControl("chkEmpRole");

                if (checkbox.Checked == true)
                {
                    HiddenField hidCode = (HiddenField)gvEmpRole.Rows[i].FindControl("hidEmpRoleId");

                    if (Codelist != "")
                    {
                        Codelist += ",'" + hidCode.Value + "'";
                    }
                    else
                    {
                        Codelist += "'" + hidCode.Value + "'";
                    }

                }
            }

            if (Codelist != "")
            {

                string respstr = "";

                APIpath = APIUrl + "/api/support/DeleteEmpRoleList";

                using (var wb = new WebClient())
                {
                    var data = new NameValueCollection();

                    data["EmpRoleIdList"] = Codelist;


                    var response = wb.UploadValues(APIpath, "POST", data);

                    respstr = Encoding.UTF8.GetString(response);
                }

                int? cou = JsonConvert.DeserializeObject<int?>(respstr);




            }
            else
            {
                hidIdList.Value = "";
                return false;
            }

            hidIdList.Value = "";
            return true;


        }
        public void Hide_Section()
        {
            showUserLogin(false);
            showRole(false);
        }
        public void sectionLoad_UserLogin ()
        {

            showSection_UserLogin.CssClass = "btn btn-primary";
            showUserLogin(true);

           //// BindddlSearchOrderStatus_All();
          //  BindddlSearchCamCate_All();
           // LoadOrder_All();

        }
        public void sectionLoad_Role()
        {

            showSection_Role.CssClass = "btn btn-primary";
            showRole(true);

            //BindddlSearchOrderStatus_All();
          //  BindddlSearchCamCate_All();
          //  LoadOrder_All();

        }
        public void btnSecondary()
        {
            if (check_UserLogin == true) { }
            else { showSection_UserLogin.CssClass = "btn-8bar-disable"; }

            if (check_Role == true) { }
            else { showSection_Role.CssClass = "  btn-8bar-disable2"; }
        }


        public void showUserLogin(Boolean show)
        {
            btnSecondary();
            showSection_UserLogin.CssClass = "btn btn-primary";


            Section_UserLogin.Visible = show;
        }

        public void showRole(Boolean show)
        {
            btnSecondary();
            showSection_Role.CssClass = "btn btn-primary";

           
            Section_Role.Visible = show;
        }

        protected void loadEmpRole()
        {
            List<EmpRole> lEmpInfo = new List<EmpRole>();

            lEmpInfo = GetEmpRoleByCriteria();

            gvEmpRole.DataSource = lEmpInfo;
            gvEmpRole.DataBind();

        }
        
        protected void loadddlRole(DropDownList ddlRole)
        {
            string respstr = "";

            APIpath = APIUrl + "/api/support/ListRoleNotInEmpRoleByCriteria";

            using (var wb = new WebClient())
            {
                var data = new NameValueCollection();

                data["EmpCode"] = (Request.QueryString["EmpCode"] != null) ? Request.QueryString["EmpCode"].ToString() : "";


                var response = wb.UploadValues(APIpath, "POST", data);

                respstr = Encoding.UTF8.GetString(response);
            }

            List<RoleInfo> lRoleInfo = JsonConvert.DeserializeObject<List<RoleInfo>>(respstr);


            ddlRole.DataSource = lRoleInfo;

            ddlRole.DataTextField = "RoleName";

            ddlRole.DataValueField = "RoleCode";

            ddlRole.DataBind();

            ddlRole.Items.Insert(0, new ListItem("กรุณาเลือก-------------------------------", "-99"));



        }
        protected Boolean validateInsertEmpRole()
        {
            Boolean flag = true;


            if (ddlRole.SelectedValue == "-99")
            {
                flag = false;
                lblRole.Text = MessageConst._MSG_PLEASEINSERT + " Role";
            }
            else
            {
                flag = (flag == false) ? false : true;
                lblRole.Text = "";
            }

            return flag;
        }
        protected List<EmpRole> GetEmpRoleByCriteria()
        {
            string respstr = "";

            APIpath = APIUrl + "/api/support/ListEmpRoleByCriteria";

            using (var wb = new WebClient())
            {
                var data = new NameValueCollection();

                data["EmpCode"] = (Request.QueryString["EmpCode"] != null) ? Request.QueryString["EmpCode"].ToString() : "";
                data["rowOFFSet"] = "0";
                data["rowFetch"] = "100";


                var response = wb.UploadValues(APIpath, "POST", data);
                respstr = Encoding.UTF8.GetString(response);
            }

            List<EmpRole> lEmpInfo = JsonConvert.DeserializeObject<List<EmpRole>>(respstr);
            return lEmpInfo;
        }


        protected void loadEmp()
        {
            List<EmpInfo> lEmpInfo = new List<EmpInfo>();
        
            lEmpInfo = GetEmpMasterByCriteria();
            if (lEmpInfo.Count > 0)
            {
                foreach (var emp in lEmpInfo)
                {
                    if (emp.ActiveFlag == "Y")
                    {
                        emp.ActiveFlagName = "Active";
                    }
                    else
                    {
                        emp.ActiveFlagName = "Inactive";
                    }
                }
                lblEmpCodeIns.Text = lEmpInfo[0].EmpCode;
               
                lblEmpFNameTHIns.Text = lEmpInfo[0].EmpFname_TH;
             
                lblEmpLNameTHIns.Text = lEmpInfo[0].EmpLname_TH;
                lblMobileIns.Text = lEmpInfo[0].Mobile;
                lblBUIns.Text = lEmpInfo[0].BuName;
                lblEmailIns.Text = lEmpInfo[0].Mail;
                lblExtensionid.Text = lEmpInfo[0].ExtensionID;
                hidUserLoginId.Value = lEmpInfo[0].UserLoginId;
                txtusernameIns.Text = lEmpInfo[0].Username;
                txtPasswordIns.Text = lEmpInfo[0].Password;
            }

        }

     
        protected List<EmpInfo> GetEmpMasterByCriteria()
        {
            string respstr = "";

            APIpath = APIUrl + "/api/support/ListEmpByCriteria";

            using (var wb = new WebClient())
            {
                var data = new NameValueCollection();
                 
                data["EmpCode"] = (Request.QueryString["EmpCode"] != null) ? Request.QueryString["EmpCode"].ToString() : "";

               

                var response = wb.UploadValues(APIpath, "POST", data);
                respstr = Encoding.UTF8.GetString(response);
            }

            List<EmpInfo> lEmpInfo = JsonConvert.DeserializeObject<List<EmpInfo>>(respstr);
            return lEmpInfo;
        }

        #endregion

        #region binding

        #endregion

       
    }
}