﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SaleCycle.Common
{
    public static class StaticField
    {
        public static readonly int PageSize = 15;
        public static readonly string SS_EmployeeLogin = "employeelogin";
        public static readonly string SS_ReportData = "module.report.require.data";
    }
}
