﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaleCycle.Common
{
    public class StatusConst
    {
        #region Finish status

        public static string _DISTRIBUTE_CR_DOC = "_DISTRIBUTE_CR_DOC";

        public static string _DISTRIBUTE_ED_DOC = "_DISTRIBUTE_ED_DOC";

        public static string _DISTRIBUTE_CN_DOC = "_DISTRIBUTE_CN_DOC";

        public static string _DISTRIBUTE_CP_DOC = "_DISTRIBUTE_CP_DOC";

        public static string _DISTRIBUTE_CH_DOC = "_DISTRIBUTE_CH_DOC";

        public static string _REJECT_DOC_CODE = "_REJECT_DOC";

        public static string _CANCEL_HOLDER_DOC_CODE = "_CANCEL_HOLDER_DOC";

        public static string _CREATE_CONTROL_DOC_CODE = "_CREATE_CONTROL_DOC";

        public static string _REJECT_REQ_CODE = "_REJECT_REQ";

        #endregion


    }
}