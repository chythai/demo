﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SaleCycle.DTO
{
    public class UserLoginInfo
    {
        public int? Id { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public string EmpFname_TH { get; set; }
        public string EmpLname_TH { get; set; }
        public DateTime? CreateDate { get; set; }
        public String CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string EmpCode { get; set; }
        public string EmpCodeTemp { get; set; }
        public string AgentId { get; set; }
        public string RefCode { get; set; }
        public string RefUserName { get; set; }

    }
}